import numpy as np
import matplotlib.pyplot as plt


before_x = []
before_y = []

after_x = []
after_y = []

data_before = open("Si_before", "r")
data_after = open("Si_after_Inter", "r")

for line in data_before:

	a,b= line.split('	')#remember to delete the empty lines in the data
	ai = float(a)
	bi = float(b)
	
	before_x.append(bi)
	before_y.append(ai)


before_x = np.asarray(before_x)
before_y = np.asarray(before_y)


for line in data_after:
	a,b= line.split('	') #remember to delete the empty lines in the data
	ai = float(a)
	bi = float(b)
	
	after_x.append(bi)
	after_y.append(ai)
	

#convert the arrays to np.arrays so that they can be used in the plot function later on.
after_x = np.asarray(after_x)
after_y = np.asarray(after_y)


plt.xlabel('wavelengths')
plt.ylabel('Values')
plt.title('Si interpolation Im')

plt.subplot(111)
plt.plot(before_y,before_x) #plot the result, horizontal axis is xi and vertical is yi //plt.plot(z, w, 'ro') for dots


plt.subplot(111)
plt.plot(after_y,after_x, ',') #plot the result, horizontal axis is xi and vertical is yi //plt.plot(z, w, 'ro') for dots
plt.show() #show the result














